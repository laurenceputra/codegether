/**
 * Module dependencies.
 */
var express = require('express')
  , routes = require('./routes')
  , io = require('socket.io') //socket.io
  , mongo = require('./models/mongodb.js') // mongodb
  , redisstore = require('connect-redis')(express) // redis sessions
  , redis = require('./models/redis.js') // redis
  , fb = require('./routes/facebook.js')
  , tw = require('./routes/twitter.js')
  , everyauth = require('everyauth');


var app = module.exports = express.createServer();
var sio = io.listen(app);
// Configuration

var RedisStore = require('connect-redis')(express);
var sessionstore = new RedisStore;

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  // redis session store.
  app.use(express.cookieParser());
  app.use(express.session({ secret: "5{6?8j6^@%$R^Q+", store: sessionstore, key: 'express.sid' }));
  app.use(everyauth.middleware());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});
app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});
app.configure('production', function(){
  app.use(express.errorHandler());
});


//socket.io session handshake
var parseCookie = require('connect').utils.parseCookie;
sio.set('authorization', function (data, accept) {
    // check if there's a cookie header
    if (data.headers.cookie) {
        // if there is, parse the cookie
        data.cookie = parseCookie(data.headers.cookie);
        // note that you will need to use the same key to grad the
        // session id, as you specified in the Express setup.
        data.sessionID = data.cookie['express.sid'];
        
         sessionstore.get(data.sessionID, function (err, session) {
            if (err || !session) {
                // if we cannot grab a session, turn down the connection
                accept('Error', false);
            } else {
                // save the session data and accept the connection
                data.session = session;
                accept(null, true);
            }
        });
        
    } else {
       // if there isn't, turn down the connection with a message
       // and leave the function.
       return accept('No cookie transmitted.', false);
    }
    // accept the incoming connection
    accept(null, true);
});
//upon connection. link socket to session id.
sio.sockets.on("connection", function(socket){
	socket.handshake.session.socket = socket;
	socket.on("enterroom", function(obj){		
		//check if room exits
		//todo
		var room = obj.roomname;
		socket.join(room);
		socket.emit("roomentered", "success");
	});

	socket.on("leaveroom", function(obj){		
		console.log(obj);
		socket.emit("roomleft", "success");
	});
	
	socket.on("outgoingmessage", function(msg){
		broadcastmessagetoroom(msg.msgbody, msg.roomname);
	});
	
	broadcastmessagetoroom = function(message, room){
		sio.sockets.in(room).emit("incomingmessage", message);
	}
});


// Routes
app.get('/', function(req, res){
	routes.index(req, res);
});


// API CALLS
//return list of chatrooms that match the location
app.post('/location', function(req,res){
	var loc = req.params.loc;
	db.chatroomsbylocation(loc, function(err, result){
		if (err){
			console.log(err);
			return;
		}
		return res.json(result);
	});
});
app.post('/chatroom/new', function(req, res){
	var loc = req.params.loc;
	var chatroomlabel = req.prams.loc;
	var userid = req.sessions.userid;
	db.createnewchatroom(chatroomlabel, loc, user, function(err, result){
		if (err) {
			console.log(err);
			return;
		}
		return req.json(result);
	});
});

//returns data of the chatroom
app.get('/room/:chatroom', function(req, res){
	db.chatroom(req.params.chatroom, function(err, result){
		if (err){
			console.log(err);
			return;
		}
		return res.json(result);
	});
});
everyauth.helpExpress(app);
app.listen(3000);


console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

