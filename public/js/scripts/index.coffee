define ['jquery', 'underscore', 'backbone', 'text!templates/msg.underscore'], ($, _, Backbone, msgTemplate) ->
	App = {}
	App.Room = {}
	App.Message = {}
	App.User = {}
	window.App = App

	App.User.model = Backbone.Model.extend
		defaults:
			name: "John Doe" #yes, you're dead
			nick: null
			thumbnail: null #img url?
	App.User.view = Backbone.View.extend
		model: App.User.model
		initialize: () ->
			_.bindAll @, 'render'
		template: {}#TODO		
		render: () ->

	App.User.collection = Backbone.Collection.extend
		model: App.User.model
		url: '/:room/users'
		initialize: () ->


	App.Message.model = Backbone.Model.extend
		defaults:
			room: null#an App.Room
			timestamp: null
			message: null
			type: null # {0:text, 1:image, 2:code, 3:markdown, ...}
			user: null # App.User.model
		initialize: () ->
	App.Message.view = Backbone.Model.extend
		el: 'li'
		className: 'chat-entry'
		model: App.Message.model
		initialize: () ->
			_.bindAll @, 'render'
		template: _.template msgTemplate
		render: () ->
			data =
				type: @model.get 'type'
				message: @model.get 'message'
				user: @model.get('user').get('nick')
			$el.html(@template data)
	App.Message.collection = Backbone.Collection.extend
		model: App.Message.model





	App.Room.model = Backbone.Model.extend
		url: '/:room'
		defaults:
			createdAt: null
			createdBy: null
			allowed: null
			name: "New Chat Room"
			welcomeMsg: null
			messages: null#a collection of App.message
			users: null#a collection of App.user
		initialize: (options) ->
			@users = new App.User.collection()
			@users.bind 'add', options.newUserHandler
			@users.fetch
				success: () -> options.successHandler "Users were successfully retreived."
				error: () -> options.errorHandler "Failed to retreive users."
			@messages = new App.Message.collection()
			@messages.bind 'add', options.newMessageHandler
			@messages.fetch
				success: () -> options.successHandler "Messages were successfully retreived."
				error: () -> options.errorHandler "Failed to retreive messages."
	
	App.Room.view = Backbone.View.extend
		el: 'div#chat-wrapper'
		model: App.Room.model
		initialize: () ->
			that = @
			_.bindAll @, 'render', 'notifyError', 'notifySuccess', 'addNewMessage', 'addNewUser', 'scrollChatView'
			@model = new App.Room.model
				newMessageHandler: @addNewMessage
				newUserHandler: @addNewUser
				successHandler: @notifySuccess
				errorHandler: @notifyError
			@model.fetch
				success: @notifySuccess "Chat room retreived successfully =)"
				error: @notifyError "Failed to retreive chat room =("
			@delegateEvents @events
			@w = #wrappers
				$msgs: $('#chat-contents', $el)
				$users: $('#chat-participants', $el)
		events: {

		}
		scrollChatView: () ->
			@w.msgs.animate scrollTop: @w.msgs.prop('scrollHeight') - @w.msgs.height(), 600
		render: () ->
			#TODO::
		addNewMessage: (msg) ->
			#takes in App.Message.model
			view = new App.Message.view
				model: msg
			@w.$msgs.append view.render()
			@scrollChatView()
		addNewUser: (user) ->
			#takes in App.User.model
			view = new App.User.view
				model: msg
			@w.$users.append view.render()
		notifyError: (text) ->
			console.log "ERROR :: #{text}"
		notifySuccess: (text) ->
			console.log "SUCCESS :: #{text}"
