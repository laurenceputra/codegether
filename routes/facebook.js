// /auth/facebook to login
// /logout to logout

const fbId = "335549073162689";
const fbSecret = "1f556c965b79f171a06563a5c2366d6e";
const fbCallbackAddress= "http://localhost:3000/signin";

var everyauth = require('everyauth');
var mongo = require('../models/mongodb.js');

everyauth.facebook
  .appId(fbId)
  .appSecret(fbSecret)
  .scope('email')
  .handleAuthCallbackError( function (req, res) {
    // If a user denies your app, Facebook will redirect the user to
    // /auth/facebook/callback?error_reason=user_denied&error=access_denied&error_description=The+user+denied+your+request.
    // This configurable route handler defines how you want to respond to
    // that.
    // If you do not configure this, everyauth renders a default fallback
    // view notifying the user that their authentication failed and why.
  })
  .findOrCreateUser( function (session, accessToken, accessTokExtra, fbUserMetadata) {
    // find or create user logic goes here
    //mongo.createnewuser()
    var user = {};
    user.facebookid = fbUserMetadata.id;
    user.name = fbUserMetadata.name;
    user.email = fbUserMetadata.email;
    user.username = fbUserMetadata.username;
    user.photo = "https://graph.facebook.com/"+fbUserMetadata.id+"/picture";

    //console.log(fbUserMetadata);

    mongo.getuserbyemail(user, function(x, auser){
      console.log(auser);
      if(auser.length == 0){
          mongo.createnewuser(user, function(x, y){return "NOOB"});
          mongo.getuserbytwitterscreenname(user, function(x, buser){
            session.username = fbUserMetadata.name;
            session.photo = "https://graph.facebook.com/"+fbUserMetadata.id+"/picture";
            session.userid = buser._id;
          })
      }
      else{
          session.username = fbUserMetadata.name;
          session.photo = "https://graph.facebook.com/"+fbUserMetadata.id+"/picture";
          session.userid = auser._id;
      }

        
      return user;
    });
    return user;
    
  })
  .redirectPath('/');

