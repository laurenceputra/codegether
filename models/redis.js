var redis = require("redis"),
	client = redis.createClient();
	
client.on("error", function(err){
	console.log("Error: " + err);
});

//redis

//entry and exit of chatrooms
exports.enterchatroom = function(chatroom, user, callback){
	var key = "chatroomusers:" + chatroom;
	client.sadd(key, user, callback);
};
exports.leavechatroom = function(chatroom, user, callback){
	var key = "chatroomusers:" + chatroom;
	client.srem(key, user, callback);
}

//messages
exports.postmessage = function(chatroom, message, callback){
	var key = "chatroommessages:" + chatroom;
	client.rpush(key, message, callback);
};

//sockets
exports.bindusertosocket = function(user, socket, callback){
	var key = "usersocket:" + user;
	client.set(key, socket, callback);
};

exports.getsocketfromuser = function(user, callback){
	var key = "usersocket:" + user;
	client.get(key, callback);
};