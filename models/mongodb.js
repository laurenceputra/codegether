var mongo = require('mongodb');
var Server = mongo.Server;
var	Db = mongo.Db;

//sets up a configuration for the connection & auto_reconnect tells the driver to retry sending a command to the server if there is a failure
var server = new Server('localhost', 27017, {auto_reconnect: true});

var db = new Db('chatdb', server);
	
db.open(function(err, db) {
  if(!err) {
	console.log("Connected to mongoDB");
  }
  //ensure geospatial index
  db.collection('chatrooms', function(err, collection){
  	if (err) return console.log(err);
  	collection.ensureIndex({ loc : '2d'});
  });

exports.all = function(callback){
	db.collection('chatrooms', function(err, collection){
		if (err) {
			console.log(err);
			return callback(err, {});
		}		
		collection.find().limit(20).toArray(function(err, result){
			if (err) {
				console.log(err);
				return callback(err, {});
			}
			
			//callback.
			callback(null, result)
		});
	});
}
// returns an array of chatrooms given a location
exports.chatroomsbylocation = function(location, callback){
	db.collection('chatrooms', function(err, collection){
		if (err){
			console.log(err);
			return callback(err, {});
		}		
		collection.find({loc: {$near: location}}).toArray(function(err, result){
			if (err){
				console.log(err);
				return callback(err, {});
			}		
			return callback(null, result);
		});
	});
}
// returns an array of messages given a chatroom
exports.messages = function(chatroom, callback){
	db.collection('messages', function(err, collection){
		if (err){
			console.log(err);
			return callback(err, {});
		}
		collection.find({chatroom: chatroom}).toArray(function(err, result){
			if (err){
				console.log(err);
				return callback(err, {});
			}
			return callback(null, result);
		});
	});
}
// creates a chatroom
exports.createnewchatroom = function(chatroomlabel, loc, user, callback){
	db.collection('chatrooms', function (err, collection){
		if (err) {
			console.log(err);
			return callback(err, {});
		}
		
		//db obj
		var chatroomobj = {};
		chatroomobj.name = chatroomlabel;
		chatroomobj.loc = loc;
		chatroomobj.createdby = user;
		chatroomobj.timestamp = new Date();
		
		collection.insert(chatroomobj, {safe: true}, function(err ,result){
			if (err){
				console.log(err);
				return callback(err,{});
			}
			return callback(null, result);
		});
	});
}
// creates a user account
exports.createnewuser = function(user, callback){
	db.collection('users', function(err, collection){
		if (err){
		 console.log(err);
		 return callback(err, {});
		}
		collection.insert(user, {safe: true}, function(err, result){
			if (err){
				console.log(err);
				return callback(err, {});
			}
			return callback(null, result);
		});
	});
};
exports.getuserbyemail = function(user, callback){
        db.collection('users', function(err, collection){
                if (err){
                         console.log(err);
                         return callback(err, {});
                }
                collection.find({email : user.email}).toArray(function(err, result){
                        if (err){
                                console.log(err);
                                return callback(err, {});
                        }
                        return callback(null, result);
                });
        });
};

exports.getuserbytwitterscreenname = function(user, callback){
        db.collection('users', function(err, collection){
                if (err){
                         console.log(err);
                         return callback(err, {});
                }

                collection.find({username : user.screen_name}).toArray(function(err, result){
                        if (err){
                                console.log(err);
                                return callback(err, {});
                        }
                        return callback(null, result);
                });
        });
};


/*
exports.createnewuser({ name: "adsf", email : "wrong92@gmail.com"}, function(err, callback){
	console.log(err);
	console.log(callback);
});
exports.getuser({email : 'wrong92@gmail.com'}, function(err ,callback){
	console.log(err);
	console.log(callback);
});
*/

});
